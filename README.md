# W-HUB API

## Installation

- Step 1:  `npm i`
- Step 2:  change `src/config.ts` (database connection, etc)
- Step 3:  `npm run build`
- Step 4:  `npm run db:migrate`
- Step 5:  `npm run start:prod`