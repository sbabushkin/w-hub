import * as Knex from 'knex';
import * as knexConfig from './knexfile';

const knexConfigOptions = knexConfig.development;

export default Knex(knexConfigOptions);
