import * as nconf from 'nconf';

nconf.env({
  separator: '__',
  whitelist: [
    'DEBUG',
    'DB_DRIVER',
    'DB_NAME',
    'DB_USER',
    'DB_PWD',
    'DB_HOST',
    'DB_PORT',
    'APP_PORT',
    'PASS',
    'CORS',
  ],
})
  .defaults({
    DB_NAME: 'u0804191_default',
    DB_DRIVER: 'mysql',
    DB_USER: 'u0804191_default',
    DB_PWD: 'g4F!1xcQ',
    DB_HOST: 'localhost',
    DB_PORT: 5432,
    APP_PORT: 3005,
    PASS: 'yesboss',
    CORS: 'http://localhost:3003,http://localhost:4567,http://campus.w-hub.ru,https://www.test-cors.org/'
  });

const conf = nconf.get();
export default conf;