import config from './config';

const {
  DATABASE_URL,
  DB_DRIVER,
  DB_NAME,
  DB_USER,
  DB_PWD,
  DB_HOST,
  DB_PORT,
} = config;

export const development = {
  client: DB_DRIVER,
  debug: true,
  connection: {
    database: DB_NAME,
    user: DB_USER,
    password: DB_PWD,
    host: DB_HOST,
  },
  migrations: {
    tableName: 'knex_migrations',
  },
};

export const production = {
  client: DB_DRIVER,
  debug: true,
  connection: {
    database: DB_NAME,
    user: DB_USER,
    password: DB_PWD,
    host: DB_HOST,
  },
  migrations: {
    tableName: 'knex_migrations',
  },
};

