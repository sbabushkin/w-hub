import * as Knex from 'knex';

exports.up = async (knex: Knex, Promise: any) => {
  const exists = await knex.schema.hasTable('user');
  if (!exists) {
    return knex.schema.createTable('user', (t) => {
      t.increments('id').primary();
      t.string('name', 250);
      t.string('email', 250);
    });
  }
}

exports.down = async (knex: Knex, Promise: any) => {
  const exists = await knex.schema.hasTable('user');
  if (exists) {
    return knex.schema.dropTable('user');
  }
}

