import { isEmail } from 'validator';

export const validateName = (name: string): string => {
  if (!name) {
    throw new Error('Поле "Имя" - обязательное');
  }
  return name.trim();
}

export const validateEmail = (email: string): string => {
  if (!email) {
    throw new Error('Поле "Email" - обязательное');
  }
  if (!isEmail(email)) {
    throw new Error(`Вы ввели не валидный адрес электронной почты: ${email}`);
  }
  return email.trim();
}