import * as Router from 'koa-router';
import { register, getAll } from '../controllers/user.controller';

const router = new Router();
router
  .get('/user', getAll)
  .post('/user', register);

export const routes = () => router.routes();
export const allowedMethods = () => router.allowedMethods();