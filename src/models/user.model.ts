import { Model } from 'objection';

class User extends Model {
  static tableName = 'user';
  id: number;
  name: string;
  email: string;

  static jsonSchema = {
    type: 'object',
    properties: {
      id: { type: 'integer', maxLength: 50 },
      name: { type: 'string', maxLength: 250 },
      email: { type: 'string', maxLength: 250 },
    },
  }
}

export default User;