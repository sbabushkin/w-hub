import User from '../models/user.model';
import { Context } from 'koa';
import { validateEmail, validateName } from '../helpers/validators';
import config from '../config';

export const getAll = async (ctx: Context) => {
  const query = ctx.request.query;
  if (query.pass && query.pass === config.PASS) { // simple temporary auth
    ctx.body = await User.query();
  } else {
    throw new Error('Доступ запрещен');
  }
}

export const register = async (ctx: Context) => {
  const postedUser = ctx.request.body;
  console.log(postedUser);
  const name: string = validateName(postedUser.name);
  const email: string = validateEmail(postedUser.email);

  const isExistUser = await User.query().findOne({ 'email': email });
  if (isExistUser) {
    throw new Error(`Пользователь с адресом электронной почты ${postedUser.email} уже загеристрирован`);
  } else {
    const result = await User.query().insert({ name, email });
    ctx.body = result;
  }
}