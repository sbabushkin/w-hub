import { Model } from 'objection';
import * as Koa from 'koa';
import * as koaBody from 'koa-body';
import * as koaBodyParser from 'koa-bodyparser';
import * as koaCors from 'koa-cors';

import * as knexInstance from './knexInstanse';
import config from './config';
import err from './middlewares/error';
import { routes, allowedMethods } from './middlewares/routes';

const koaOptions: koaCors.Options = {
  credentials: true,
  origin: true,
  //(request: Koa.Request) => request.origin 
  //config.CORS.split(',').includes(request.origin) ? request.origin : ''
};

Model.knex(knexInstance.default);

const app = new Koa();
app.use(err);
app.use(koaBodyParser());
// app.use(koaBody());
app.use(koaCors(koaOptions));
app.use(routes());
app.use(allowedMethods());

app.listen(config.APP_PORT, () => {
  console.log('app listening at port %d', config.APP_PORT);
});
